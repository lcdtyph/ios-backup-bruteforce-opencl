#ifndef __PBKDF2_H__
#define __PBKDF2_H__

#include <CL/cl2.hpp>
#include <memory>
#include <vector>
#include <stdint.h>

class Pbkdf2OpenclHelper {
public:
    using Bytes = std::vector<uint8_t>;
    ~Pbkdf2OpenclHelper();

    void SetParameters(const Bytes &salt, size_t iterations, size_t dk_len = 32);
    void Process(const std::vector<std::string> &passwords, std::vector<Bytes> &results);
    void Process(const std::vector<Bytes> &passwords, std::vector<Bytes> &results);

    size_t GetDerivedKeyLength() const;

    static std::shared_ptr<Pbkdf2OpenclHelper>
        GetExecutor(const cl::Context &context, const std::string &hash_spec);
private:
    Pbkdf2OpenclHelper() = default;

    static const size_t kMaxOutputLength;
    static const std::string kKernelFunction;

    cl::Context context_;
    cl::Program program_;
    std::vector<uint8_t> salt_;
    size_t iterations_;
    size_t dk_len_;
};

#endif // __PBKDF2_H__

