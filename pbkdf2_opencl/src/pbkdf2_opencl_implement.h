#ifndef __PBKDF2_OPENCL_IMPLEMENT_H__
#define __PBKDF2_OPENCL_IMPLEMENT_H__

#include <stdint.h>

struct InBuffer {
    uint32_t length;
    uint8_t data[32];
};

extern const char *pbkdf2_sha1_opencl;
extern const char *pbkdf2_sha256_opencl;

#endif // __PBKDF2_OPENCL_IMPLEMENT_H__

