
#include <stdexcept>
#include <array>
#include <algorithm>
#include <memory>
#include "pbkdf2_opencl/pbkdf2.h"
#include "pbkdf2_opencl_implement.h"

const size_t Pbkdf2OpenclHelper::kMaxOutputLength = 32;
const std::string Pbkdf2OpenclHelper::kKernelFunction = "pbkdf2_kernel";

Pbkdf2OpenclHelper::~Pbkdf2OpenclHelper() {
}

void Pbkdf2OpenclHelper::SetParameters(const Bytes &salt, size_t iterations, size_t dk_len) {
    if (salt.size() > 32) {
        throw std::runtime_error("salt too long");
    }
    std::fill(std::begin(salt_), std::end(salt_), 0);
    auto data = reinterpret_cast<InBuffer *>(salt_.data());
    data->length = salt.size();
    std::copy(std::begin(salt), std::end(salt), &data->data[0]);
    iterations_ = iterations;
    dk_len_ = std::min((size_t)32, dk_len);
}

void Pbkdf2OpenclHelper::Process(
    const std::vector<std::string> &passwords,
    std::vector<Bytes> &results
) {
    std::vector<Bytes> pws;
    pws.reserve(passwords.size());
    std::transform(
        std::begin(passwords), std::end(passwords),
        std::back_inserter(pws),
        [](const std::string &str) {
            return std::vector<uint8_t>{str.begin(), str.end()};
        }
    );
    Process(pws, results);
}

void Pbkdf2OpenclHelper::Process(
    const std::vector<Bytes> &passwords,
    std::vector<Bytes> &results
) {
    const size_t per_inbuf_size = sizeof(InBuffer);
    results.clear();
    if (passwords.empty()) {
        return;
    }

    size_t batch_size = passwords.size();
    size_t input_buffer_size = per_inbuf_size * batch_size;
    size_t output_buffer_size = kMaxOutputLength * batch_size;
    std::vector<uint8_t> buffer(input_buffer_size, (uint8_t)0);
    auto data = reinterpret_cast<InBuffer *>(buffer.data());
    for (size_t i = 0; i < batch_size; ++i) {
        auto buf = data + i;
        buf->length = passwords[i].size();
        if (buf->length > 32) {
            throw std::runtime_error("password too long");
        }
        std::copy(std::begin(passwords[i]), std::end(passwords[i]), &buf->data[0]);
    }
    cl::Buffer input_buffer{context_, std::begin(buffer), std::end(buffer), true};
    cl::Buffer output_buffer{context_, CL_MEM_READ_WRITE, output_buffer_size};
    cl::Buffer salt_buffer{context_, std::begin(salt_), std::end(salt_), true};

    auto queue = cl::CommandQueue{context_, 0};

    auto kernel = \
        cl::KernelFunctor<cl::Buffer, cl::Buffer, cl::Buffer, int>(program_, kKernelFunction);

    auto event = \
        kernel(
            cl::EnqueueArgs(queue, cl::NDRange(batch_size)),
            input_buffer, output_buffer,
            salt_buffer, iterations_
        );
    event.wait();

    buffer.resize(output_buffer_size);
    cl::copy(queue, output_buffer, std::begin(buffer), std::end(buffer));
    results.reserve(batch_size);
    for (size_t i = 0; i < batch_size; ++i) {
        auto data = buffer.data() + i * kMaxOutputLength;
        results.emplace_back(data, data + dk_len_);
    }
}

size_t Pbkdf2OpenclHelper::GetDerivedKeyLength() const {
    return dk_len_;
}

static const char *GetSourceFromHashSpec(const std::string &hash_spec) {
    if (hash_spec == "sha1") {
        return pbkdf2_sha1_opencl;
    } else if (hash_spec == "sha256") {
        return pbkdf2_sha256_opencl;
    }
    throw std::runtime_error("invalid hash spec: " + hash_spec);
}

std::shared_ptr<Pbkdf2OpenclHelper>
    Pbkdf2OpenclHelper::GetExecutor(const cl::Context &context, const std::string &hash_spec) {
        std::shared_ptr<Pbkdf2OpenclHelper> result{new Pbkdf2OpenclHelper};
        result->context_ = context;
        result->salt_.resize(sizeof(InBuffer));
        result->program_ = cl::Program{result->context_, GetSourceFromHashSpec(hash_spec)};
        result->program_.build(
        #if CL_HPP_TARGET_OPENCL_VERSION >= 200
            "-cl-std=CL2.0"
        #endif // CL_HPP_TARGET_OPENCL_VERSION >= 200
        );
        result->iterations_ = 0;
        result->dk_len_ = 0;
        return result;
    }

