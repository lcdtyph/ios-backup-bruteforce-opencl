#include <iostream>
#include <fstream>
#include <string>
#include <memory>
#include <cstring>
#include <cstdlib>
#include <chrono>
#include <gflags/gflags.h>
#include <CL/cl2.hpp>

#include <pbkdf2_opencl/pbkdf2.h>
#include <dp/keybag.h>

#include "utils.h"

DEFINE_string(keybag, "", "path to keybag binary data");
DEFINE_string(wordlist, "", "path to word list for brute force");
DEFINE_uint32(batch, 0, "batch size while brute force");
DEFINE_bool(list_device, false, "list available devices");
DEFINE_uint32(device, 0, "which device to be used");

std::shared_ptr<std::string> StartBruteforce(
    std::ifstream &ifs,
    uint32_t batch_size,
    std::shared_ptr<BackupKeyBag> keybag,
    std::shared_ptr<Pbkdf2OpenclHelper> first_executor,
    std::shared_ptr<Pbkdf2OpenclHelper> second_executor
);

int main(int argc, char *argv[])
{
    google::ParseCommandLineFlags(&argc, &argv, true);

    std::vector<cl::Device> devices;
    cl::Platform::getDefault().getDevices(CL_DEVICE_TYPE_ALL, &devices);
    if (devices.size() == 0) {
        std::cerr << "cannot find opencl device" << std::endl;
        std::exit(-1);
    }
    if (devices.size() <= FLAGS_device) {
        std::cerr << "invalid device number: " << FLAGS_device << std::endl;
        std::exit(-1);
    }

    if (FLAGS_list_device) {
        for (auto &dev : devices) {
            std::cout << dev.getInfo<CL_DEVICE_NAME>() << std::endl;
        }
        return 0;
    }

    auto keybag = GetKeyBagFromFile(FLAGS_keybag);
    if (keybag == nullptr) {
        std::cerr << "invalid keybag file" << std::endl;
        std::exit(-1);
    }

    std::ifstream ifs(FLAGS_wordlist);
    if (!ifs) {
        std::cerr << "invalid word list file" << std::endl;
        std::exit(-1);
    }

    auto first_option = keybag->GetFirstKdfOption();
    auto second_option = keybag->GetSecondKdfOption();

    auto picked_device = devices.at(FLAGS_device);
    std::cout << "Picked device: " << picked_device.getInfo<CL_DEVICE_NAME>() << std::endl;
    auto max_item_size = picked_device.getInfo<CL_DEVICE_MAX_WORK_ITEM_SIZES>();
    std::cout << "Device max work item size: " << max_item_size[0] << std::endl;
    if (FLAGS_batch == 0) {
        FLAGS_batch = max_item_size[0];
        std::cout << "Batch size is reset to device max work item size: " << FLAGS_batch << std::endl;
    }

    cl::Context context(picked_device);
    auto first_executor = Pbkdf2OpenclHelper::GetExecutor(context, first_option.hash_spec);
    auto second_executor = Pbkdf2OpenclHelper::GetExecutor(context, second_option.hash_spec);

    first_executor->SetParameters(first_option.salt, first_option.iters);
    second_executor->SetParameters(second_option.salt, second_option.iters);

    std::cout << "Context initialized, will start immediately\n" << std::endl;
    std::shared_ptr<std::string> result;
    auto start_time = std::chrono::system_clock::now();
    result = \
        StartBruteforce(
            ifs, FLAGS_batch, keybag,
            first_executor,
            second_executor
        );
    auto end_time = std::chrono::system_clock::now();
    if (result != nullptr) {
        std::cout << "Password found: " << *result << std::endl;
    } else {
        std::cout << "No Password found" << std::endl;
    }
    std::cout << "Elapsed time in "
              << std::chrono::duration<double>(end_time - start_time).count()
              << "s in total." << std::endl;

    google::ShutDownCommandLineFlags();

    return 0;
}

std::shared_ptr<std::string> StartBruteforce(
    std::ifstream &ifs,
    uint32_t batch_size,
    std::shared_ptr<BackupKeyBag> keybag,
    std::shared_ptr<Pbkdf2OpenclHelper> first_executor,
    std::shared_ptr<Pbkdf2OpenclHelper> second_executor
) {

    std::vector<std::string> lines(batch_size);
    while (ifs) {
        for (auto itr = lines.begin(); itr != lines.end(); ++itr) {
            if (!std::getline(ifs, *itr)) {
                lines.erase(itr, lines.end());
                break;
            }
        }
        size_t this_batch_size = lines.size();
        if (this_batch_size == 0) {
            break;
        }

        auto start_time = std::chrono::system_clock::now();
        std::cout << "Processing " << this_batch_size << " passcodes ..." << std::endl;
        std::vector<std::vector<uint8_t>> intermediate_results;
        std::cout << "Stage 1..." << std::flush;
        first_executor->Process(lines, intermediate_results);

        std::vector<std::vector<uint8_t>> final_results;
        std::cout << "Stage 2..." << std::endl;
        second_executor->Process(intermediate_results, final_results);
        auto end_time = std::chrono::system_clock::now();

        std::cout << "Done! Elapsed time in " << std::fixed
                  << std::chrono::duration<double>(end_time - start_time).count()
                  << "s" << std::endl;
        std::cout << "Verifying ..." << std::endl;
        std::vector<uint8_t> temp_key(second_executor->GetDerivedKeyLength());
        for (size_t i = 0; i < this_batch_size; ++i) {
            if (keybag->UnlockWithPasscodeKey(final_results[i])) {
                return std::make_shared<std::string>(lines[i]);
            }
        }
        std::cout << "No matching password...\n" << std::endl;
    }
    return nullptr;
}

