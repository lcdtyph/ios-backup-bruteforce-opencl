
#include <fstream>
#include <iterator>
#include <memory>
#include <algorithm>
#include <iostream>

#include "utils.h"

std::shared_ptr<BackupKeyBag> GetKeyBagFromFile(const std::string &kb_file) {
    std::ifstream ifs(kb_file, std::ios::binary);
    if (!ifs) return nullptr;
    ifs.seekg(0, std::ios::end);
    auto total = ifs.tellg();
    ifs.seekg(0, std::ios::beg);
    std::vector<uint8_t> buf(total);
    ifs.read((char *)buf.data(), total);
    std::cout << "Keybag data size: " << total << std::endl;
    return BackupKeyBag::FromByteSequence(buf);
}

void DumpBytes(std::ostream &os, const uint8_t *data, size_t len) {
    for (size_t i = 0; i < len; ++i) {
        os.width(2);
        os.fill('0');
        os << std::hex << (static_cast<uint32_t>(data[i]) & 0xff);
    }
    os << std::dec << std::endl;
}

