#ifndef __UTILS_H__
#define __UTILS_H__

#include <memory>
#include <string>

#include <dp/keybag.h>

std::shared_ptr<BackupKeyBag> GetKeyBagFromFile(const std::string &kb_file);

void DumpBytes(std::ostream &os, const uint8_t *data, size_t len);

#endif

